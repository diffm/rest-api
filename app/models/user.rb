class User < ApplicationRecord
    has_secure_password

    belongs_to :profile, optional: true

    # enum role: %i[employee hr admin].freeze
    enum role: { employee: "employee", admin: "admin", hr: "hr" }
    
    validates :username, presence: true
    validates :email, presence: true
    validates :role, presence: true
    # validates :number, presence: true
    # validates :password, presence: true

    scope :except_admin, -> { where.not(role: 'admin') }
end
