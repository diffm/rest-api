class Profile < ApplicationRecord
    has_one :user

    validates :username, presence: true
    validates :email, presence: true
    validates :number, presence: true
end
