class ApplicationController < ActionController::API
    include Response
    include ExceptionHandler

    before_action :authorize_request
    attr_reader :current_user

    private

    def authorize_request
        @current_user = (AuthorizeApiRequest.new(request.headers).call)[:user]
    end

    def authorize_roles(roles)
        return if Array.wrap(roles).include?(@current_user.role)

        raise ExceptionHandler::UnauthorizedRole.new('Not available for the role')
    end
    def authorize_admin
        authorize_roles('admin')
    end
    def authorize_hr
        authorize_roles('hr')
    end

    def authorize_admin_or_hr
        authorize_roles(['hr', 'admin'])
    end

    def authorize_employee
        authorize_roles('employee')
    end
end
