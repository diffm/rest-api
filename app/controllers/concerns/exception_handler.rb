module ExceptionHandler
    extend ActiveSupport::Concern

    class UnauthorizedRole < StandardError ;end
    class MissingToken < StandardError ;end
    class UnauthorizedShowAccess < StandardError ;end

    included do
        # Define custom handlers
        rescue_from ActiveRecord::RecordInvalid, with: :four_twenty_two
        rescue_from ExceptionHandler::UnauthorizedRole, with: :unauthorized_request
        rescue_from ExceptionHandler::MissingToken, with: :four_twenty_two
        # rescue_from ExceptionHandler::InvalidToken, with: :four_twenty_two
        rescue_from ExceptionHandler::UnauthorizedShowAccess, with: :unauthorized_request
        rescue_from ActiveRecord::RecordNotFound do |e|
          json_response({ message: e.message }, :not_found)
        end
      end
    
      private
    
      # JSON response with message; Status code 422 - unprocessable entity
      def four_twenty_two(e)
        json_response({ message: e.message }, :unprocessable_entity)
      end
    
      # JSON response with message; Status code 401 - Unauthorized
      def unauthorized_request(e)
        json_response({ message: e.message }, :unauthorized)
      end
end
