class AuthenticationController < ApplicationController
    skip_before_action :authorize_request, only: :authenticate
    def authenticate
        auth_user_data = AuthenticateUser.new(auth_params[:email], auth_params[:password]).call
        json_response(auth_user_data)
    end

    private

    def auth_params
        params.permit(:email, :password)
    end
end
