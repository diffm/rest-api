class Api::UsersController < ApplicationController
    # skip_before_action :authorize_request, only: :create

    before_action :authorize_admin, only: [:create, :admin, :destroy]
    before_action :authorize_admin_or_hr, only: [:employees]
    before_action :check_employee_id, only: [:show, :update]
    before_action :check_hr_id, only: [:show, :update]

    def create
        user = nil
        ActiveRecord::Base.transaction do
            user = User.create!(user_params)
            profile = Profile.create!(profile_attributes)
            user.profile_id = profile.id
            user.save!
        end
        auth_token = AuthenticateUser.new(user.email, user.password).call
        response = { message: Message.account_created, auth_token: auth_token }
        json_response(response, :created)
    end

    def non_admin
      page_records = User.except_admin.page(params[:page]).per(2)
      prev_page = page_records.prev_page
      next_page = page_records.next_page

      json_response({
        records: page_records,
        prev_page: prev_page,
        next_page: next_page
      })
    end

    def employees
        page_records = User.employee.page(params[:page]).per(2)
        prev_page = page_records.prev_page
        next_page = page_records.next_page
  
        json_response({
          records: page_records,
          prev_page: prev_page,
          next_page: next_page
        })
    end

    def destroy
      user = User.find(params[:id])
      user.destroy!

      head :ok
    end

    def update
        user = nil
        ActiveRecord::Base.transaction do
            user = User.find(params[:id])
            user.update!(user_params)
            profile = user.profile
            profile.update!(profile_attributes)
        end
        response = { message: Message.user_updated, user: user }
        json_response(response, :ok)
    end

    def show
        user = User.find(params[:id])
        user_attributes = user.attributes.slice("id", "username", "email", "role")
        profile_attributes = user.profile.attributes.slice("id", "username", "email", "salary", "number")

        json_response({user: user_attributes, profile: profile_attributes}, :ok)
    end

    private

    def user_params
        params.permit(
            :username,
            :email,
            :role,
            :password,
            :passwordConfirmation
        )
    end

    def profile_params
        params.permit(
            :number,
            :salary
        )
    end

    def profile_attributes
        profile_params.merge(
            username: user_params[:username],
            email: user_params[:email]
        )
    end

    def check_employee_id
        return if @current_user.role != 'employee'
        return if @current_user.id.to_i == params[:id].to_i

        raise ExceptionHandler::UnauthorizedShowAccess.new('Cannot view this user')
    end

    def check_hr_id
        return if !@current_user.hr?
        editing_user = User.find(params[:id])
        return if (@current_user.id.to_i == params[:id].to_i) || editing_user.employee?

        raise ExceptionHandler::UnauthorizedShowAccess.new('Cannot view this user')
    end
end
