class AddProfileIdToUsers < ActiveRecord::Migration[6.1]
  def change
    add_reference :users, :profile, foreign_key: true
    remove_column :profiles, :to_user, :string
    rename_column :profiles, :name, :username
  end
end
