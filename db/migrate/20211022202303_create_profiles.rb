class CreateProfiles < ActiveRecord::Migration[6.1]
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :email
      t.string :number
      t.string :salary
      t.string :to_user

      t.timestamps
    end
  end
end
