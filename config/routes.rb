Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  
  post 'auth/login', to: 'authentication#authenticate'
  
  namespace :api do
    resources :profiles
  
    resources :users do 
      collection do
        get '/non_admin', to: 'users#non_admin'
        get '/employees', to: 'users#employees'
      end
    end
  end
end
